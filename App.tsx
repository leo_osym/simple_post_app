import React, {useEffect, useRef, useState} from 'react';
import createApolloClient from 'graphql/client';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import Posts from 'screens/Posts';
import {
  DefaultTheme,
  NavigationContainer,
  NavigationContainerRef,
  ParamListBase,
} from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import CreatePost from 'screens/CreatePost';
import DetailPost from 'screens/DetailPost';
import {ApolloProvider} from '@apollo/client';

const AppStack = createStackNavigator();

function AppNavigation() {
  return (
    <AppStack.Navigator
      initialRouteName={'Feed'}
      detachInactiveScreens={false}
      screenOptions={{
        ...TransitionPresets.SlideFromRightIOS,
        headerStyle: {
          shadowColor: 'transparent',
        },
        headerBackTitleVisible: false,
      }}>
      <AppStack.Screen name={'Feed'} component={Posts} />
      <AppStack.Screen
        name={'CreatePost'}
        component={CreatePost}
        options={{
          headerShown: true,
          headerTitle: 'Create Post',
        }}
      />
      <AppStack.Screen
        name={'DetailPost'}
        component={DetailPost}
        options={{
          headerShown: true,
          headerTitle: 'Detail Post',
        }}
      />
    </AppStack.Navigator>
  );
}

const App = () => {
  const [apolloClient, setApolloClient] = useState<any>(null);
  const navRef = useRef<NavigationContainerRef<ParamListBase> | null>(null);

  useEffect(() => {
    const initializeApollo = async () => {
      const client = await createApolloClient();
      setApolloClient(client);
    };
    initializeApollo();
  }, []);

  if (!apolloClient) {
    return <></>;
  }
  return (
    <ApolloProvider client={apolloClient}>
      <SafeAreaProvider>
        <NavigationContainer ref={navRef} theme={DefaultTheme}>
          <AppNavigation />
        </NavigationContainer>
      </SafeAreaProvider>
    </ApolloProvider>
  );
};

export default App;
