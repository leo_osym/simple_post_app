module.exports = {
  root: true,
  extends: [
    '@react-native-community',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
    'plugin:jest/recommended'
  ],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'jest'],
  overrides: [
    {
      files: ['**.ts', '**.tsx'],
    }
  ],
  globals: {
    element: true,
    by: true,
    device: true,
    jasmine: true,
   $: true,
   browser: true
  },
  settings: {
    'import/parsers': {
    '@typescript-eslint/parser': ['.ts', '.tsx']
  },
     'import/extensions': ['.ts', '.tsx'],
     'import/resolver': {
        'babel-module': {}
  }
},
rules: {
   'comma-dangle': ['error', 'never'],
   '@typescript-eslint/no-unused-vars': ['warn'],
   'no-console': ['error', { allow: ['warn', 'disableYellowBox'] }],
   'import/named': ['off'],
   'react-native/no-inline-styles': 2,
   'import/no-named-default': ['off'],
   'import/no-named-as-default': ['off'],
   'import/namespace': ['off'],
   'import/default': ['off'],
   'import/no-unresolved': ['off'],
   'import/no-named-as-default-member': ['off'],
   'import/order': 'off',
   'jest/no-jasmine-globals': 0,
   'jest/expect-expect': 0
  }
};