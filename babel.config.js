module.exports = {
  plugins: [
    [
      'babel-plugin-module-resolver',
      {
        alias: {},
        extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
        root: ['./src']
      }
    ],
    'graphql-tag'
  ],
  presets: ['module:metro-react-native-babel-preset']
};
