export const navigationMock = {
    addListener: (_type: string, _callback: Function) => { return () => { } },
    removeListener: (_type: string, _callback: Function) => { return () => { } },
    canGoBack: () => true,
    goBack: () => { },
    dangerouslyGetParent: () => {
        return {
            addListener: () => { return () => { } },
            dangerouslyGetParent: () => { return { dangerouslyGetParent: () => { return { addListener: () => { }, }; } }; },
            state: { routes: [] },
            isFocused: () => true,
        };
    },
    isFocused: () => true,
    reset: (_state: any) => { },
    pop: () => { },
    popToTop: () => { },
    push: (_routeName: string, _params: any) => { },
    navigate: (_routeName: string, _params?: any) => { },
    dispatch: (_action: any) => { },
    dangerouslyGetState: () => { return { index: 0 } },
    setOptions: (_options: any) => { },
    setParams: (_params: any) => { },
    replace: (_routeName: string, _params?: any) => { },
};
