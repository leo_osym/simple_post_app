export const singlePostMock = {
    __typename: 'Post',
    id: 1234,
    title: 'test title', 
    body: 'test body',
    user: {
        name: 'Leo'
    }
};

export const postsMock = [
    {
        __typename: 'Post',
        id: 1,
        title: 'test title1', 
        body: 'test body1',
        user: {
            name: 'Leo'
        }
    },
    {
        __typename: 'Post',
        id: 2,
        title: 'test title2', 
        body: 'test body2',
        user: {
                name: 'Leo'
        }
    },
    {
        __typename: 'Post',
        id: 3,
        title: 'test title2', 
        body: 'test body2',
        user: {
                name: 'Leo'
        }
    },
    {
        __typename: 'Post',
        id: 4,
        title: 'test title2', 
        body: 'test body2',
        user: {
                name: 'Leo'
        }
    },
    {
        __typename: 'Post',
        id: 5,
        title: 'test title2', 
        body: 'test body2',
        user: {
                name: 'Leo'
        }
    },
    {
        __typename: 'Post',
        id: 6,
        title: 'test title2', 
        body: 'test body2',
        user: {
                name: 'Leo'
        }
    },
    {
        __typename: 'Post',
        id: 7,
        title: 'test title2', 
        body: 'test body2',
        user: {
                name: 'Leo'
        }
    },
    {
        __typename: 'Post',
        id: 8,
        title: 'test title2', 
        body: 'test body2',
        user: {
                name: 'Leo'
        }
    },
    {
        __typename: 'Post',
        id: 9,
        title: 'test title2', 
        body: 'test body2',
        user: {
                name: 'Leo'
        }
    },
    {
        __typename: 'Post',
        id: 10,
        title: 'test title2', 
        body: 'test body2',
        user: {
                name: 'Leo'
        }
    },
    {
        __typename: 'Post',
        id: 11,
        title: 'test title2', 
        body: 'test body2',
        user: {
                name: 'Leo'
        }
    },
    {
        __typename: 'Post',
        id: 12,
        title: 'test title2', 
        body: 'test body2',
        user: {
                name: 'Leo'
        }
    }
];
