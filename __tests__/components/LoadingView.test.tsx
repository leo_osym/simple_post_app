import 'react-native';
import React from 'react';
import LoadingView from 'components/LoadingView';
import { create } from 'react-test-renderer';


describe('LoadingView test', () => {
    test('renders correctly, is shown === true', () => {
        const snapshot = create(<LoadingView isShown={true}/>);
        expect(snapshot).toMatchSnapshot();
    });
    test('renders correctly, is shown === false', () => {
        const snapshot = create(<LoadingView isShown={false}/>);
        expect(snapshot).toMatchSnapshot();
    });
});