import 'react-native';
import React from 'react';
import { fireEvent, render } from '@testing-library/react-native';
import PostItem from 'components/PostItem';
import { singlePostMock } from '../../__mocks__/posts';

describe('PostItem test', () => {
    test('renders correctly', () => {
        const snapshot = render(<PostItem post={singlePostMock} onPressEdit={()=>{}} onPressPost={()=>{}} numberOfLines={4}/>);
        expect(snapshot).toMatchSnapshot();
    });
    test('onPressPost called', () => {
        const onPressPostFunction = jest.fn();
        const { getByTestId } = render(<PostItem post={singlePostMock} onPressPost={onPressPostFunction} onPressEdit={()=>{}} numberOfLines={4}/>);
        const postButton = getByTestId(`PostItem${singlePostMock.id}`);
        fireEvent(postButton, 'onPress');
        expect(onPressPostFunction).toHaveBeenCalledWith(singlePostMock.id);
    });
    test('onEditPost called', () => {
        const onEditPostFunction = jest.fn();
        const { getByTestId } = render(<PostItem post={singlePostMock} onPressPost={()=>{}} onPressEdit={onEditPostFunction} numberOfLines={4}/>);
        const editButton = getByTestId(`PostItemEdit${singlePostMock.id}`);
        fireEvent(editButton, 'onPress');
        expect(onEditPostFunction).toHaveBeenCalledWith(singlePostMock.id);
    });
});