import 'react-native';
import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import {MockedProvider} from '@apollo/client/testing';
import Posts from 'screens/Posts';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {GetPostsDocument} from 'graphql';
import {postsMock} from '../../../__mocks__/posts';
import { navigationMock } from '../../../__mocks__/navigation';

const successMock = [
  {
    request: {
      query: GetPostsDocument,
      variables: {
        options: {paginate: {page: 0, limit: 10}}
      }
    },
    result: {
      data: {
        posts: {
          data: postsMock,
          meta: {__typename: 'PageMetadata', totalCount: postsMock?.length}
        }
      }
    }
  }
];

describe('Posts test', () => {
  it('renders without error', () => {
    const component = render(
      <MockedProvider mocks={successMock} addTypename={false}>
        <Posts navigation={navigationMock} route={{}} />
      </MockedProvider>
    );
    expect(component).toMatchSnapshot();
  });
  it('create post button press', () => {
    const navigate = jest.fn();
    const {getByTestId} = render(
      <MockedProvider mocks={successMock} addTypename={false}>
        <Posts navigation={{...navigationMock, navigate}} route={{}} />
      </MockedProvider>
    );
    const button = getByTestId(`ButtonCreatePost`);
    fireEvent(button, 'onPress');
    expect(navigate).toHaveBeenCalledWith('CreatePost', {});
  });
});
