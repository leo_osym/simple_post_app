import 'react-native';
import React from 'react';
import {cleanup, fireEvent, render} from '@testing-library/react-native';
import {MockedProvider} from '@apollo/client/testing';
import {GetPostDocument, GetPostsDocument, UpdatePostDocument} from 'graphql';
import {singlePostMock} from '../../../__mocks__/posts';
import {Alert} from 'react-native';
import CreatePost from 'screens/CreatePost';
import {CreatePostDocument} from 'graphql';
import {navigationMock} from '../../../__mocks__/navigation';

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

const updateExistingPostMockSuccess = [
  {
    request: {
      query: GetPostDocument,
      variables: {
        id: '1234'
      }
    },
    result: {
      data: {
        post: singlePostMock
      }
    }
  },
  {
    request: {
      query: UpdatePostDocument,
      variables: {
        id: '1234',
        post: {
          title: 'Test',
          body: 'Test'
        }
      }
    },
    result: {
      data: {
        post: singlePostMock
      }
    }
  },
  {
    request: {
      query: GetPostsDocument,
      variables: {
        options: {paginate: {page: 0, limit: 10}}
      },
    },
    error: new Error('An error occurred')
  }
];

const updateExistingPostMockError = [
  {
    request: {
      query: GetPostDocument,
      variables: {
        id: '1234'
      }
    },
    result: {
      data: {
        post: singlePostMock
      }
    }
  },
  {
    request: {
      query: UpdatePostDocument,
      variables: {
        id: '1234',
        post: {
          title: 'Test',
          body: 'Test'
        }
      }
    },
    error: new Error('An error occurred')
  },
  {
    request: {
      query: GetPostsDocument,
      variables: {
        options: {paginate: {page: 0, limit: 10}}
      },
    },
    error: new Error('An error occurred')
  }
];

describe('CreatePost test', () => {
  it('renders without error', () => {
    const component = render(
      <MockedProvider mocks={updateExistingPostMockSuccess} addTypename={false}>
        <CreatePost navigation={navigationMock} route={{params: {postId: '1234'}}} />
      </MockedProvider>
    );
    expect(component).toMatchSnapshot();
  });
  it('update post, success', async () => {
    const alert = jest.spyOn(Alert, 'alert');
    const {getByTestId} = render(
      <MockedProvider mocks={updateExistingPostMockSuccess} addTypename={false}>
        <CreatePost
          navigation={navigationMock}
          route={{params: {postId: '1234'}}}
        />
      </MockedProvider>
    );
    const titleInput = getByTestId('CreatePostTitle');
    const bodyInput = getByTestId('CreatePostBody');
    const saveButton = getByTestId('SavePostButton');
    fireEvent.changeText(titleInput, 'Test');
    fireEvent.changeText(bodyInput, 'Test');
    fireEvent.press(saveButton);
    jest.runAllTimers();
    expect(alert).toHaveBeenCalledWith('', 'Post updated successfully!');
  });
  test('update post, error', async () => {
    const alert = jest.spyOn(Alert, 'alert');
    const {getByTestId} = render(
      <MockedProvider mocks={updateExistingPostMockError} addTypename={false}>
        <CreatePost
          navigation={navigationMock}
          route={{params: {postId: '1234'}}}
        />
      </MockedProvider>
    );
    const titleInput = getByTestId('CreatePostTitle');
    const bodyInput = getByTestId('CreatePostBody');
    const saveButton = getByTestId('SavePostButton');
    fireEvent.changeText(titleInput, 'Test');
    fireEvent.changeText(bodyInput, 'Test');
    fireEvent.press(saveButton);
    jest.runAllTimers();
    expect(alert).toHaveBeenCalledWith("Error", "Could not update this post!");
  });
});
