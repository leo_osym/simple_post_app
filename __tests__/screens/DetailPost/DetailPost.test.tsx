import 'react-native';
import React from 'react';
import {render} from '@testing-library/react-native';
import DetailPost from 'screens/DetailPost';
import {MockedProvider} from '@apollo/client/testing';
import {GetPostDocument} from 'graphql';
import {singlePostMock} from '../../../__mocks__/posts';
import {Alert} from 'react-native';

const successMock = [
  {
    request: {
      query: GetPostDocument,
      variables: {
        id: '1234'
      }
    },
    result: {
      data: {
        post: singlePostMock
      }
    }
  }
];

const errorMock = [
  {
    request: {
      query: GetPostDocument,
      variables: {
        id: '1234'
      }
    },
    error: new Error('An error occurred')
  }
];

describe('DetailPost test', () => {
  it('renders without error', () => {
    const component = render(
      <MockedProvider mocks={successMock} addTypename={false}>
        <DetailPost navigation={{}} route={{params: {postId: '1234'}}} />
      </MockedProvider>
    );
    expect(component).toMatchSnapshot();
  });

  it('should show error UI', async () => {
    jest.spyOn(Alert, 'alert');
    const component = render(
      <MockedProvider mocks={errorMock} addTypename={false}>
        <DetailPost navigation={{}} route={{params: {postId: '1234'}}} />
      </MockedProvider>
    );
    jest.runAllTimers();
    expect(Alert.alert).toHaveBeenCalledWith('', 'An error occurred');
  });
});
