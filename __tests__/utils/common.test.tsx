import 'react-native';
import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import PostItem from 'components/PostItem';
import {singlePostMock} from '../../__mocks__/posts';
import {cleanEmptyAttributes} from '../../src/utils/common';

const rawObjects = [
  {
    a: 12,
    b: 'dfdgf',
    c: true,
    d: [1, 2, 4],
    e: {test: 'test'}
  },
  {
    a: undefined,
    b: null,
    c: '',
    d: [],
    e: {test: null},
    f: {},
  },
];

const cleanedObjects = [
  {
    a: 12,
    b: 'dfdgf',
    c: true,
    d: [1, 2, 4],
    e: {test: 'test'}
  },
  {
    c: '',
    d: [],
    e: {test: null},
    f: {}
  }
];

describe('PostItem test', () => {
  test.each([
    [rawObjects[0], cleanedObjects[0]],
    [rawObjects[1], cleanedObjects[1]],
  ])(
    'cleanEmptyAttributes, test with correct data',
    (rawObject, cleanedObject) => {
      expect(cleanEmptyAttributes(rawObject)).toEqual(cleanedObject);
    }
  );
  test.each([
    [null, {}],
    [undefined, {}],
    ['', {}],
    [0, {}],
    [[], {}],
    [{}, {}]
  ])(
    'cleanEmptyAttributes, test with incorrect data',
    (rawObject, cleanedObject) => {
      expect(cleanEmptyAttributes(rawObject)).toEqual(cleanedObject);
    }
  );
});
