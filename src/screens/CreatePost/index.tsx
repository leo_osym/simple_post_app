import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  ViewStyle,
  ScrollView,
  TextInput,
  TouchableOpacity,
  View,
  Alert
} from 'react-native';
import {NavigationAppStackRouteParams} from 'core/types';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/core';
import {Text} from 'react-native';
import {colors} from 'core/styles';
import {GetPostsDocument, useGetPostQuery} from 'graphql';
import LoadingView from 'components/LoadingView';
import {useCreatePostMutation} from 'graphql';
import {useUpdatePostMutation} from 'graphql';

interface ScreenProps {
  navigation: StackNavigationProp<NavigationAppStackRouteParams, 'CreatePost'>;
  route: RouteProp<NavigationAppStackRouteParams, 'CreatePost'>;
}

export default function CreatePost({navigation, route}: ScreenProps) {
  const {postId} = route.params;
  const {data: post, loading} = useGetPostQuery({
    variables: {id: postId || ''}
  });
  const [createPost, {loading: loadingCreatePost}] = useCreatePostMutation({
    onCompleted: () => {
      Alert.alert('', 'Post created successfully!');
      navigation.goBack();
    },
    onError: () => {
      Alert.alert('Error', 'Could not create this post!');
    }
  });
  const [updatePost, {loading: loadingUpdatePost}] = useUpdatePostMutation({
    onCompleted: () => {
      Alert.alert('', 'Post updated successfully!');
      navigation.goBack();
    },
    onError: () => {
      Alert.alert('Error', 'Could not update this post!');
    }
  });
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');

  useEffect(() => {
    if (postId) {
      navigation.setOptions({
        headerTitle: 'Update post'
      });
    }
  }, [navigation, postId]);

  useEffect(() => {
    if (post && postId) {
      setTitle(post?.post?.title || '');
      setBody(post?.post?.body || '');
    }
  }, [post, postId]);

  const onPressSave = useCallback(async () => {
    if (postId) {
      await updatePost({
        variables: {
          id: postId,
          post: {
            title,
            body
          }
        },
        refetchQueries: [
          {
            query: GetPostsDocument,
            variables: {
              options: { paginate: { page: 0, limit: 10 }}
            }
          }
        ]
      });
    } else {
      await createPost({
        variables: {
          post: {
            title,
            body
          }
        },
        refetchQueries: [
          {
            query: GetPostsDocument,
            variables: {
              options: { paginate: { page: 0, limit: 10 }}
            }
          }
        ]
      });
    }
  }, [title, body, postId, createPost, updatePost]);

  return (
    <View style={styles.container}>
      <ScrollView
        style={styles.list}
        contentContainerStyle={styles.listContainer}>
        <Text>{'Enter title:'}</Text>
        <TextInput
          testID={`CreatePostTitle`}
          accessibilityLabel={`CreatePostTitle`}
          style={styles.title}
          placeholder={'Enter title:'}
          value={title}
          onChangeText={setTitle}
        />
        <Text>{'Enter text:'}</Text>
        <TextInput
          testID={`CreatePostBody`}
          accessibilityLabel={`CreatePostBody`}
          style={styles.body}
          placeholder={'Enter text:'}
          value={body}
          onChangeText={setBody}
          multiline={true}
          numberOfLines={10}
        />
      </ScrollView>
      <View style={styles.buttonContainer}>
        <TouchableOpacity 
          testID={`SavePostButton`}
          accessibilityLabel={`SavePostButton`}
          style={styles.button} 
          onPress={onPressSave} >
          <Text style={styles.buttonText}>{'SAVE'}</Text>
        </TouchableOpacity>
      </View>
      <LoadingView isShown={loading || loadingCreatePost || loadingUpdatePost} />
    </View>
  );
}

const styles = {
  container: {
    flex: 1,
    width: '100%'
  } as ViewStyle,
  list: {
    flex: 1,
    width: '100%'
  } as ViewStyle,
  listContainer: {
    paddingHorizontal: 16,
    paddingTop: 10
  } as ViewStyle,
  title: {
    width: '100%',
    padding: 16,
    fontSize: 18,
    borderColor: colors.gray,
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10
  } as ViewStyle,
  body: {
    width: '100%',
    height: 300,
    padding: 16,
    fontSize: 18,
    borderColor: colors.gray,
    borderWidth: 1,
    borderRadius: 8
  } as ViewStyle,
  buttonContainer: {
    width: '100%',
    height: 66,
    paddingHorizontal: 16,
    paddingBottom: 16
  } as ViewStyle,
  button: {
    flex: 1,
    backgroundColor: colors.primary,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center'
  } as ViewStyle,
  buttonText: {
    fontSize: 20,
    fontWeight: '500',
    color: colors.white
  } as ViewStyle
};
