import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  Alert,
  RefreshControl,
  View,
  ViewStyle,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Image,
} from 'react-native';
import {NavigationAppStackRouteParams} from 'core/types';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/core';
import LoadingView from 'components/LoadingView';
import {colors} from 'core/styles';
import {Text} from 'react-native';
import Animated from 'react-native-reanimated';
import {ImageStyle} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {screenWidth} from 'core/constants';
import {Post, useGetPostsQuery} from 'graphql';
import PostItem from 'components/PostItem';

const AnimatedTouchable = Animated.createAnimatedComponent(TouchableOpacity);
const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const plus = require('assets/icons/plus.png');

interface ScreenProps {
  navigation: StackNavigationProp<NavigationAppStackRouteParams, 'Posts'>;
  route: RouteProp<NavigationAppStackRouteParams, 'Posts'>;
}

export default function Posts({navigation, route}: ScreenProps) {
  const [refresh, setIsRefresh] = useState(false);
  const [refetchLoading, setRefetchLoading] = useState(false);
  const [onMomentumBegin, setOnMomentumBegin] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const {data: posts, loading, refetch: refetchPosts, fetchMore} = useGetPostsQuery({
    variables: {
      options: { paginate: { page: 0, limit: 10 }}
    },
    fetchPolicy: 'network-only'
  });
  const offset = useRef(new Animated.Value(0));

  const onRefresh = useCallback(async () => {
    setIsRefresh(true);
    await refetchPosts({options: { paginate: { page: 0, limit: 10}}});
    setCurrentPage(1);
    setIsRefresh(false);
  }, [setIsRefresh, setCurrentPage]);

  const onPressPost = useCallback(
    (postId: string | null | undefined) => {
      if (postId) {
        navigation.navigate('DetailPost', {postId});
      }
    },
    [navigation]
  );

  const onPressEdit = useCallback(
    (postId: string | null | undefined) => {
      if (postId) {
        navigation.navigate('CreatePost', {postId});
      }
    },
    [navigation]
  );

  const onEndReached = useCallback(async ({ distanceFromEnd }) => {
    if(!onMomentumBegin && posts?.posts?.meta?.totalCount && (posts?.posts?.data?.length || 0) < posts?.posts?.meta?.totalCount){
      const nextPage = currentPage + 1;
      setRefetchLoading(true);
      await fetchMore({
        variables: {
          options: {
            paginate: { page: nextPage, limit: 10}
          }
        },
        updateQuery: (prev, { fetchMoreResult }) => {
          let exists = false;
          if(prev?.posts?.data?.length && 
            fetchMoreResult?.posts?.data?.length && 
            prev?.posts?.data[prev?.posts?.data?.length -1]?.id ===
            fetchMoreResult?.posts?.data[fetchMoreResult?.posts?.data?.length - 1]?.id){
              exists = true;
            }
          if (!fetchMoreResult || exists) return prev;
          const newFeed = {
            posts: {
              ...fetchMoreResult?.posts,
              data: prev?.posts?.data?.concat(fetchMoreResult?.posts?.data || [])
            }
          };
          return newFeed;
        }
      });
      setCurrentPage(nextPage);
      setRefetchLoading(false);
      setOnMomentumBegin(true);
    }
  }, [loading, posts, currentPage]);

  return (
    <View style={styles.container}>
      <AnimatedFlatList
        contentContainerStyle={{flexGrow: 1}}
        style={styles.list}
        keyExtractor={(item: Post) => item?.id}
        data={posts?.posts?.data || []}
        initialNumToRender={10}
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: {
                  y: (y: Animated.Value<number>) =>
                    Animated.set(offset.current, y),
                },
              },
            },
          ],
          {useNativeDriver: true}
        )}
        scrollEventThrottle={5}
        onEndReachedThreshold={0.5}
        onMomentumScrollBegin={() => setOnMomentumBegin(false)}
        onEndReached={({distanceFromEnd}: {distanceFromEnd: number}) => {
          if (!onMomentumBegin && !loading && !refetchLoading) {
            onEndReached({distanceFromEnd});
          }
        }}
        refreshControl={
          <RefreshControl
            enabled={true}
            refreshing={refresh}
            onRefresh={() => onRefresh()}
            tintColor={colors.gray}
          />
        }
        ListEmptyComponent={
          <View style={styles.emptyContainer}>
            <Text style={{textAlign: 'center'}}>
              {'The list is empty. Add some posts to continue'}
            </Text>
          </View>
        }
        ListFooterComponent={() =>
          refetchLoading ? (
            <View style={styles.footer}>
              <ActivityIndicator size={'large'} color={colors.primary}/>
            </View>
          ) : null
        }
        renderItem={({item}: {item: Post}) => (
          <PostItem
            post={item}
            numberOfLines={2}
            onPressPost={onPressPost}
            onPressEdit={onPressEdit}
          />
        )}
      />
      <LoadingView isShown={loading} />
      <AnimatedTouchable
        testID={`ButtonCreatePost`}
        accessibilityLabel={`ButtonCreatePost`}
        style={{
          ...styles.label,
          transform: [
            {
              translateY: Animated.interpolate(offset?.current, {
                inputRange: [0, 200],
                outputRange: [0, 200],
                extrapolate: Animated.Extrapolate.CLAMP,
              }),
            },
          ],
        }}
        onPress={() => navigation.navigate('CreatePost', {})}>
        <Image style={styles.icon} source={plus} />
      </AnimatedTouchable>
    </View>
  );
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  } as ViewStyle,
  list: {
    width: '100%',
  } as ViewStyle,
  emptyContainer: {
    height: '100%',
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignSelf: 'center',
  } as ViewStyle,
  footer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 50,
  } as ViewStyle,
  icon: {
    width: 30,
    height: 30,
  } as ImageStyle,
  label: {
    width: 56,
    height: 56,
    borderRadius: 56 / 2,
    position: 'absolute',
    left: screenWidth / 2 - 56 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: colors.black,
    backgroundColor: colors.primary,
    borderWidth: 1,
    bottom: 50
  } as ViewStyle,
};
