import React from 'react';
import {
  Alert,
  View,
  ViewStyle,
} from 'react-native';
import {NavigationAppStackRouteParams} from 'core/types';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/core';
import LoadingView from 'components/LoadingView';
import {ImageStyle} from 'react-native';
import PostItem from 'components/PostItem';
import {ScrollView} from 'react-native-gesture-handler';
import { useGetPostQuery } from 'graphql';

interface ScreenProps {
  navigation: StackNavigationProp<NavigationAppStackRouteParams, 'DetailPost'>;
  route: RouteProp<NavigationAppStackRouteParams, 'DetailPost'>;
}

export default function DetailPost({navigation, route}: ScreenProps) {
  const { postId } = route.params;
  const {data: post, loading} = useGetPostQuery({
    variables: { id: postId },
    onError: (error) => {
      Alert.alert('', error?.message)
    }
  });

  return (
    <View style={styles.container}>
      <ScrollView>
        <PostItem
          numberOfLines={10}
          onPressEdit={() => {}}
          onPressPost={() => {}}
          post={post?.post || {}}
        />
      </ScrollView>
      <LoadingView isShown={loading} />
    </View>
  );
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
  } as ViewStyle,
  list: {
    width: '100%',
  } as ViewStyle,
  emptyContainer: {
    height: '100%',
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignSelf: 'center',
  } as ViewStyle,
  footer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  } as ViewStyle,
  icon: {
    width: 30,
    height: 30,
  } as ImageStyle,
};
