export type NavigationAppStackRouteParams = {
  Posts: undefined;
  DetailPost: {postId: string};
  CreatePost: {postId?: string};
};
