export const colors = {
  black: '#000',
  white: '#fff',
  gray: '#818C99',
  postGradientFrom: '#1B2128',
  postGradientTo: '#151B21',
  modalBackground: 'rgba(0,0,0,0.6)',
  transparent: 'transparent',
  primary: '#30C3E4',
};
