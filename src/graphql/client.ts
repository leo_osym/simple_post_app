import {InMemoryCache, defaultDataIdFromObject} from 'apollo-cache-inmemory';
import {ApolloClient} from 'apollo-client';
import {concat} from 'apollo-link';
import {onError} from 'apollo-link-error';
import {createHttpLink} from 'apollo-link-http';
import {HOST} from 'core/constants';
import {persistCache} from 'apollo-cache-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {PersistedData, PersistentStorage} from 'apollo-cache-persist/types';

export default async () => {
  const httpLink = createHttpLink({
    uri: HOST,
  });

  const errorLink = onError(
    ({graphQLErrors, networkError, operation, response}) => {
      console.warn({graphQLErrors, networkError, operation, response});
    },
  );

  const cache = new InMemoryCache({
    dataIdFromObject: (responseObject: any) => {
      return defaultDataIdFromObject(responseObject);
    },
  });

  const client = new ApolloClient({
    cache,
    defaultOptions: {
      watchQuery: {
        fetchPolicy: 'cache-first',
      },
    },
    link: concat(errorLink, httpLink),
  });

  persistCache({
    cache,
    storage: AsyncStorage as PersistentStorage<PersistedData<any>>,
  });

  return client;
};
