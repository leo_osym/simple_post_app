import React from 'react';
import {ActivityIndicator, View, ViewStyle} from 'react-native';

import {colors} from 'core/styles';

interface Props {
  isShown: boolean;
}

export default function LoadingView({isShown}: Props) {
  return (
    <>
      {isShown ? (
        <View style={styles.refresh}>
          <ActivityIndicator size={'large'} />
        </View>
      ) : null}
    </>
  );
}

const styles = {
  refresh: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    backgroundColor: colors.modalBackground,
    zIndex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  } as ViewStyle,
};
