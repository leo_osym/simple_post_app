import React from 'react';
import {View, ViewStyle, Text, TouchableOpacity, Image} from 'react-native';

import {colors} from 'core/styles';
import {Post} from 'graphql';
import {ImageStyle} from 'react-native';
import {TextStyle} from 'react-native';

const more = require('assets/icons/edit.png');

interface Props {
  post: Post;
  numberOfLines?: number;
  onPressPost: (id: string | null | undefined) => void;
  onPressEdit: (id: string | null | undefined) => void;
}

export default function PostItem({
  post,
  onPressEdit,
  onPressPost,
  numberOfLines,
}: Props) {
  return (
    <TouchableOpacity 
      style={styles.main} 
      testID={`PostItem${post.id}`}
      accessibilityLabel={`PostItem${post.id}`}
      onPress={() => onPressPost(post?.id)}>
      <View style={styles.headContainer}>
        <Text style={styles.user}>{post?.user?.name}</Text>
        <TouchableOpacity 
          onPress={() => onPressEdit(post?.id)}
          testID={`PostItemEdit${post.id}`}
          accessibilityLabel={`PostItemEdit${post.id}`}>
          <Image source={more} style={styles.icon} />
        </TouchableOpacity>
      </View>
      <Text style={styles.title}>{post?.title}</Text>
      <Text style={styles.body} numberOfLines={numberOfLines}>
        {post?.body}
      </Text>
    </TouchableOpacity>
  );
}

const styles = {
  refresh: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    backgroundColor: colors.modalBackground,
    zIndex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  } as ViewStyle,
  icon: {
    width: 25,
    height: 20,
    tintColor: colors.black,
  } as ImageStyle,
  body: {
    fontSize: 18,
  } as TextStyle,
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  } as TextStyle,
  user: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'blue',
    marginRight: 40,
  } as TextStyle,
  main: {
    width: '100%',
    backgroundColor: 'lightblue',
    padding: 16,
    borderBottomWidth: 2,
    borderBottomColor: colors.gray,
  } as ViewStyle,
  headContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  } as ViewStyle,
};
