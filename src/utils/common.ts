export function cleanEmptyAttributes(attributeObject: {[key: string]: any}) {
  if(attributeObject && typeof attributeObject === 'object' && !Array.isArray(attributeObject)){
    for (let propName in attributeObject) {
      if (attributeObject[propName] === null || attributeObject[propName] === undefined) {
        delete attributeObject[propName];
      }
    }
    return attributeObject;
  }
  return {};
}
